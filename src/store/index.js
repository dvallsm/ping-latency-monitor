import Vue from "vue";
import Vuex from "vuex";

// import example from './module-example'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      isPlaying: false,
      urls: []
    },

    getters: {
      getIsPlaying: state => state.isPlaying,
      urls: state => state.urls
    },
    mutations: {
      mutateIsPlaying (state, newState) {
        state.isPlaying = newState
      },

      mutateUrls (state, newState) {
        state.urls = newState
      }
    },
    actions: {
      togglePlayPause ({ commit, state }, payload) {
        commit('mutateIsPlaying', !state.isPlaying)
      },

      setUrls ({ commit }, payload) {
        commit('mutateUrls', payload)
      }
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
