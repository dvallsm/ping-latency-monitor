export default {
  File: 'File',
  Options: 'Options',
  IPURL: 'IP/Domain',
  ResetOptions: 'Reset stored IPS/URL',
  Lost: 'Lost',
  Latency: 'Latency'
};
