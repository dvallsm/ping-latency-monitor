export default {
  File: 'Archivo',
  Options: 'Options',
  IPURL: 'IP/Dominio',
  ResetOptions: 'Reiniciar IPS/URL almacenadas',
  Lost: 'Perdidos',
  Latency: 'Latencia'
}