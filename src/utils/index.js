function makeUrlFriendly(text) {
  return text.toLowerCase()
    .trim()
    .replace(/ /gm, "-")
    .replace(/[#|@~€¬!"·$%&()=¿?*ºª<>:,;_'¡+^´ç¨Ç{}\\]/gm, "")
    .replace(/[àèìòùäëïöüáéíóúãõñ]/gm, "")
}

export { makeUrlFriendly }
export default { makeUrlFriendly }