import ping from 'ping'
import { exec } from 'child_process'
import { promisify } from 'util'

const execute = promisify(exec)

function pingHost(host) {
  return ping.promise.probe(host).then(function (pingResponse) {
    console.log('alive: ' + pingResponse.alive);
    console.log('time (ms): ' + pingResponse.time);
  }).catch(error => console.log(error))
}

function systemPing(host) {
  return execute(`ping ${host} -n 1`)
    .then(({ error, stdout, stderr }) => {
      if (!error) {
        const ms = Number(stdout.split(" = ").pop().replace("ms", ""))
        if (!ms) throw new Error()
        return ms
      } else {
        throw new Error(`${error}: ${stderr}`)
      }
    })
}

export { pingHost, systemPing }
export default { pingHost, systemPing }